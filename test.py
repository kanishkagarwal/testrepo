from time import sleep
from flask import Flask

app = Flask(__name__)

@app.route('/')
def dosomething():
  print('do something', flush=True)
  print('done')

  return 'We did it?'

app.run(host="0.0.0.0", port=8080, debug=True)

